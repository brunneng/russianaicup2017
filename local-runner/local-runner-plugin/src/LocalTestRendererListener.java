import model.Game;
import model.World;

import java.awt.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Locale;
import java.util.Scanner;

import static java.lang.StrictMath.abs;
import static java.lang.StrictMath.round;

public final class LocalTestRendererListener {
   private static final File file = new File("F:/Projects/aicontests/raic2017/local-runner/draw.log");

   private Graphics graphics;
   private World world;
   private Game game;

   private int canvasWidth;
   private int canvasHeight;

   private double left;
   private double top;
   private double width;
   private double height;

   private int logTick = -1;

   public void beforeDrawScene(Graphics graphics, World world, Game game, int canvasWidth, int canvasHeight,
                               double left, double top, double width, double height) {
      updateFields(graphics, world, game, canvasWidth, canvasHeight, left, top, width, height);

//        graphics.setColor(Color.BLACK);
//        drawRect(100.0D, 150.0D, 900.0D, 950.0D);
//
//        for (Vehicle vehicle : world.getVehicles()) {
//            double radius = vehicle.getRadius();
//            double diameter = radius * 2.0D;
//            drawRect(vehicle.getX() - radius, vehicle.getY() - radius, diameter, diameter);
//        }
   }

   public void afterDrawScene(Graphics graphics, World world, Game game, int canvasWidth, int canvasHeight,
                              double left, double top, double width, double height) {
      updateFields(graphics, world, game, canvasWidth, canvasHeight, left, top, width, height);

      try {
         Scanner s = new Scanner(file);
         s.useLocale(Locale.FRANCE);

         logTick = -1;
         while (s.hasNextLine()) {
            parseAndDraw(s.nextLine());
//            if (logTick > 0 && logTick < world.getTickIndex() || logTick > world.getTickIndex() + 10) {
//               break;
//            }
         }

      } catch (Exception e) {
         File errorsFile = new File("F:/Projects/aicontests/raic2017/local-runner/errors.log");
         errorsFile.delete();
         try (PrintWriter pw = new PrintWriter(errorsFile)) {
            e.printStackTrace(pw);
            pw.flush();
         } catch (FileNotFoundException e1) {
            e1.printStackTrace();
         }
      }
      setColor(Color.BLACK);
   }

   public void setColor(Color color) {
      graphics.setColor(color);
   }

   public void parseAndDraw(String line) {
      Scanner s = new Scanner(line);
      String command = s.next();
      if (command.equals("tick")) {
         logTick = Integer.parseInt(s.next());
      }
      else if (command.equals("action")) {
         parseAction(s);
      }
      else if (command.equals("group")) {
         parseGroup(s);
      }
   }

   public void parseAction(Scanner s) {
      String actionType = s.next();
      if (actionType.equals("move")) {
         Point2D fromPos = readPoint(s);
         Point2D toPos = readPoint(s);

         setColor(Color.RED);
         drawLine(fromPos.x, fromPos.y, toPos.x, toPos.y);
      }
      else if (actionType.equals("arc")) {
         Point2D fromPos = readPoint(s);
         Point2D toPos = readPoint(s);

         Point2D rotationCenter = readPoint(s);
         double startAngle = s.nextDouble();
         double rotationAngle = s.nextDouble();

         setColor(Color.PINK);
         drawLine(fromPos.x, fromPos.y, toPos.x, toPos.y);

//         setColor(Color.RED);
//         drawArc(rotationCenter.x, rotationCenter.y, rotationCenter.distanceTo(fromPos),
//                 toDegrees(startAngle), toDegrees(rotationAngle));
      }
   }

   int toDegrees(double a) {
      return (int) (a * 57.2958);
   }

   public void parseGroup(Scanner s) {
      Point2D pos = readPoint(s);
      double radius = s.nextDouble();
      Point2D axisEnd1 = readPoint(s);
      Point2D axisEnd2 = readPoint(s);

      setColor(Color.GREEN);
      drawCircle(pos.x, pos.y, radius);
      drawLine(axisEnd1.x, axisEnd1.y, axisEnd2.x, axisEnd2.y);
   }

   public Point2D readPoint(Scanner s) {
      double x = s.nextDouble();
      double y = s.nextDouble();
      return new Point2D(x, y);
   }

   private void updateFields(Graphics graphics, World world, Game game, int canvasWidth, int canvasHeight,
                             double left, double top, double width, double height) {
      this.graphics = graphics;
      this.world = world;
      this.game = game;

      this.canvasWidth = canvasWidth;
      this.canvasHeight = canvasHeight;

      this.left = left;
      this.top = top;
      this.width = width;
      this.height = height;
   }

   private void drawLine(double x1, double y1, double x2, double y2) {
      Point2I lineBegin = toCanvasPosition(x1, y1);
      Point2I lineEnd = toCanvasPosition(x2, y2);

      graphics.drawLine(lineBegin.getX(), lineBegin.getY(), lineEnd.getX(), lineEnd.getY());
   }

   private void fillCircle(double centerX, double centerY, double radius) {
      Point2I topLeft = toCanvasPosition(centerX - radius, centerY - radius);
      Point2I size = toCanvasOffset(2.0D * radius, 2.0D * radius);

      graphics.fillOval(topLeft.getX(), topLeft.getY(), size.getX(), size.getY());
   }

   private void drawCircle(double centerX, double centerY, double radius) {
      Point2I topLeft = toCanvasPosition(centerX - radius, centerY - radius);
      Point2I size = toCanvasOffset(2.0D * radius, 2.0D * radius);

      graphics.drawOval(topLeft.getX(), topLeft.getY(), size.getX(), size.getY());
   }

   private void fillArc(double centerX, double centerY, double radius, int startAngle, int arcAngle) {
      Point2I topLeft = toCanvasPosition(centerX - radius, centerY - radius);
      Point2I size = toCanvasOffset(2.0D * radius, 2.0D * radius);

      graphics.fillArc(topLeft.getX(), topLeft.getY(), size.getX(), size.getY(), startAngle, arcAngle);
   }

   private void drawArc(double centerX, double centerY, double radius, int startAngle, int arcAngle) {
      Point2I topLeft = toCanvasPosition(centerX - radius, centerY - radius);
      Point2I size = toCanvasOffset(2.0D * radius, 2.0D * radius);

      graphics.drawArc(topLeft.getX(), topLeft.getY(), size.getX(), size.getY(), startAngle, arcAngle);
   }

   private void fillRect(double left, double top, double width, double height) {
      Point2I topLeft = toCanvasPosition(left, top);
      Point2I size = toCanvasOffset(width, height);

      graphics.fillRect(topLeft.getX(), topLeft.getY(), size.getX(), size.getY());
   }

   private void drawRect(double left, double top, double width, double height) {
      Point2I topLeft = toCanvasPosition(left, top);
      Point2I size = toCanvasOffset(width, height);

      graphics.drawRect(topLeft.getX(), topLeft.getY(), size.getX(), size.getY());
   }

   private void drawPolygon(Point2D... points) {
      int pointCount = points.length;

      for (int pointIndex = 1; pointIndex < pointCount; ++pointIndex) {
         Point2D pointA = points[pointIndex];
         Point2D pointB = points[pointIndex - 1];
         drawLine(pointA.getX(), pointA.getY(), pointB.getX(), pointB.getY());
      }

      Point2D pointA = points[0];
      Point2D pointB = points[pointCount - 1];
      drawLine(pointA.getX(), pointA.getY(), pointB.getX(), pointB.getY());
   }

   private Point2I toCanvasOffset(double x, double y) {
      return new Point2I(x * canvasWidth / width, y * canvasHeight / height);
   }

   private Point2I toCanvasPosition(double x, double y) {
      return new Point2I((x - left) * canvasWidth / width, (y - top) * canvasHeight / height);
   }

   private static final class Point2I {
      private int x;
      private int y;

      private Point2I(double x, double y) {
         this.x = toInt(round(x));
         this.y = toInt(round(y));
      }

      private Point2I(int x, int y) {
         this.x = x;
         this.y = y;
      }

      private Point2I() {
      }

      public int getX() {
         return x;
      }

      public void setX(int x) {
         this.x = x;
      }

      public int getY() {
         return y;
      }

      public void setY(int y) {
         this.y = y;
      }

      private static int toInt(double value) {
         @SuppressWarnings("NumericCastThatLosesPrecision") int intValue = (int) value;
         if (abs((double) intValue - value) < 1.0D) {
            return intValue;
         }
         throw new IllegalArgumentException("Can't convert double " + value + " to int.");
      }
   }

   private static final class Point2D {
      private double x;
      private double y;

      private Point2D(double x, double y) {
         this.x = x;
         this.y = y;
      }

      private Point2D() {
      }

      public double getX() {
         return x;
      }

      public void setX(double x) {
         this.x = x;
      }

      public double getY() {
         return y;
      }

      public void setY(double y) {
         this.y = y;
      }

      public double distanceTo(Point2D other) {
         double dx = (x - other.x);
         double dy = (y - other.y);
         return Math.hypot(dx, dy);
      }
   }
}
