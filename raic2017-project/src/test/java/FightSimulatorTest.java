import model.VehicleType;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * User: goodg_000
 * Date: 11.11.2017
 * Time: 20:22
 */
public class FightSimulatorTest {

   private FightSimulator fightSimulator;

   @Before
   public void setup() {
      DamageMatrix dm = new DamageMatrix(
              new double[] {0, 0, 100, 90, 100},
              new double[] {0, 100, 80, 80, 60},
              new double[] {50, 70, 40, 60, 80},
              new double[] {20, 70, 40, 80, 60});
      fightSimulator = new FightSimulator(dm);
   }

   @Test
   public void testTankBattle1() {
      UnitsCounts u1 = new UnitsCounts();
      u1.add(VehicleType.TANK, 25);

      UnitsCounts u2 = new UnitsCounts();
      u2.add(VehicleType.TANK, 25);

      double kdr = fightSimulator.fight(u1, u2).getKilledMinusLost();
      Assert.assertTrue(kdr == 0);
   }

   @Test
   public void testTankBattle2() {
      UnitsCounts u1 = new UnitsCounts();
      u1.add(VehicleType.TANK, 25);

      UnitsCounts u2 = new UnitsCounts();
      u2.add(VehicleType.TANK, 24);

      double kdr = fightSimulator.fight(u1, u2).getKilledMinusLost();
      Assert.assertTrue(kdr > 0);
   }

   @Test
   public void testTankBattle3() {
      UnitsCounts u1 = new UnitsCounts();
      u1.add(VehicleType.TANK, 20);

      UnitsCounts u2 = new UnitsCounts();
      u2.add(VehicleType.TANK, 10);

      double kdr = fightSimulator.fight(u1, u2).getKilledMinusLost();
      Assert.assertTrue(kdr > 5);
   }

   @Test
   public void testTankVsBmpBattle1() {
      UnitsCounts u1 = new UnitsCounts();
      u1.add(VehicleType.TANK, 10);

      UnitsCounts u2 = new UnitsCounts();
      u2.add(VehicleType.IFV, 20);

      double kdr = fightSimulator.fight(u1, u2).getKilledMinusLost();
      Assert.assertTrue(kdr > 10);
   }

   @Test
   public void testTankVsBmpBattle2() {
      UnitsCounts u1 = new UnitsCounts();
      u1.add(VehicleType.TANK, 5);

      UnitsCounts u2 = new UnitsCounts();
      u2.add(VehicleType.IFV, 20);

      double kdr = fightSimulator.fight(u1, u2).getKilledMinusLost();
      Assert.assertTrue(kdr < 0);
   }

   @Test
   public void testHealearsVsFightersBattle2() {
      UnitsCounts u1 = new UnitsCounts();
      u1.add(VehicleType.ARRV, 5);

      UnitsCounts u2 = new UnitsCounts();
      u2.add(VehicleType.FIGHTER, 5);

      double kdr = fightSimulator.fight(u1, u2).getKilledMinusLost();
      Assert.assertTrue(kdr == 0);
   }

   @Test
   public void testTankBattlePerformance1() {
      UnitsCounts u1 = new UnitsCounts();
      u1.add(VehicleType.TANK, 25);

      UnitsCounts u2 = new UnitsCounts();
      u2.add(VehicleType.TANK, 25);
      u2.add(VehicleType.ARRV, 25);

      long start = System.currentTimeMillis();
      int count = 100000;
      for (int i = 0; i < count; ++i) {
         double kdr = fightSimulator.fight(u1, u2).getKilledMinusLost();
         Assert.assertTrue(kdr > 0);
      }
      long time = System.currentTimeMillis() - start;
      System.out.println(String.format("testTankBattlePerformance1 (%s): %sms ", count, time));
   }

   @Test
   public void testFightersVsMixedGroup1() {
      UnitsCounts u1 = new UnitsCounts();
      u1.add(VehicleType.FIGHTER, 30);

      UnitsCounts u2 = new UnitsCounts();
      u2.add(VehicleType.HELICOPTER, 5);
      u2.add(VehicleType.TANK, 5);
      u2.add(VehicleType.IFV, 5);

      double kdr = fightSimulator.fight(u1, u2).getKilledMinusLost();
      Assert.assertTrue(kdr < 0);
   }

   @Test
   public void testSingleFightersVsMixedGroup1() {
      UnitsCounts u1 = new UnitsCounts();
      u1.add(VehicleType.FIGHTER, 30);

      UnitsCounts u2 = new UnitsCounts();
      u2.add(VehicleType.HELICOPTER, 5);
      u2.add(VehicleType.TANK, 5);
      u2.add(VehicleType.IFV, 5);

      double kdr = fightSimulator.fightSingleRound(u1, u2);
      Assert.assertTrue(kdr > 0);
   }

   @Test
   public void testTanksVsTanksAndHealers1() {
      UnitsCounts u1 = new UnitsCounts();
      u1.add(VehicleType.TANK, 3);

      UnitsCounts u2 = new UnitsCounts();
      u2.add(VehicleType.TANK, 3);
      u2.add(VehicleType.ARRV, 3);
      double kdr = fightSimulator.fight(u1, u2).getKilledMinusLost();
      Assert.assertTrue(kdr > 0);
   }

   @Test
   public void testBmpVsHealers1() {
      UnitsCounts u1 = new UnitsCounts();
      u1.add(VehicleType.IFV, 1);

      UnitsCounts u2 = new UnitsCounts();
      u2.add(VehicleType.ARRV, 25);
      double kdr = fightSimulator.fight(u1, u2).getKilledMinusLost();
      Assert.assertTrue(kdr == 0);
   }

   @Test
   public void testTanksVsTanksWithFactory1() {
      UnitsCounts u1 = new UnitsCounts();
      u1.add(VehicleType.TANK, 10);

      UnitsCounts u2 = new UnitsCounts();
      u2.add(VehicleType.TANK, 15);
      boolean canDefend = fightSimulator.fight(u1, u2, VehicleType.TANK, null).getU1().isNotEmpty();
      Assert.assertTrue(canDefend);
   }
}
