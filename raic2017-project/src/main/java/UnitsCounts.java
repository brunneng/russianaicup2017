import model.VehicleType;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * User: goodg_000
 * Date: 11.11.2017
 * Time: 19:03
 */
public class UnitsCounts {
   double[] unitsCounts = new double[VehicleType.values().length];

   public UnitsCounts() {

   }

   public UnitsCounts(List<BattleUnit> units) {
      for (BattleUnit u : units) {
         add(u.getType(), u.getDurabilityPercent());
      }
   }

   public UnitsCounts(UnitsCounts other) {
      add(other);
   }

   public void add(VehicleType vehicleType, double count) {
      unitsCounts[vehicleType.ordinal()] += count;
   }

   public double get(VehicleType vehicleType) {
      return unitsCounts[vehicleType.ordinal()];
   }

   public void add(UnitsCounts other) {
      for (int i = 0; i < unitsCounts.length; ++i) {
         unitsCounts[i] += other.unitsCounts[i];
      }
   }

   public double getTotalUnitsCount() {
      double res = 0;
      for (double d : unitsCounts) {
         res += d;
      }
      return res;
   }

   public boolean damage(VehicleType vehicleType, double damage) {
      boolean res = false;
      int i = vehicleType.ordinal();
      unitsCounts[i] -= damage;
      if (unitsCounts[i] <= 0) {
         unitsCounts[i] = 0;
         res = true;
      }
      return res;
   }

   public boolean isNotEmpty() {
      for (int i = 0; i < unitsCounts.length; ++i) {
         if (unitsCounts[i] > 0) {
            return true;
         }
      }
      return false;
   }

   public boolean isEmpty() {
      for (int i = 0; i < unitsCounts.length; ++i) {
         if (unitsCounts[i] > 0) {
            return false;
         }
      }
      return true;
   }

   public List<VehicleType> getNonZeroVehicleTypes() {
      List<VehicleType> res = new ArrayList<>();
      for (VehicleType t : VehicleType.values()) {
         if (get(t) > 0) {
            res.add(t);
         }
      }
      return res;
   }

   public void clear() {
      Arrays.fill(unitsCounts, 0);
   }

   public void negate() {
      for (int i = 0; i < unitsCounts.length; ++i) {
         unitsCounts[i] = -unitsCounts[i];
      }
   }

   public UnitsCounts copy() {
      return new UnitsCounts(this);
   }
}
