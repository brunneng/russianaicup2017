import model.VehicleType;

/**
 * User: goodg_000
 * Date: 30.11.2017
 * Time: 23:16
 */
public class KillProductionStrategyInfo extends GroupStrategyInfo {

   private long targetFacilityId;
   private VehicleType unitTypeToCounter;

   public long getTargetFacilityId() {
      return targetFacilityId;
   }

   public void setTargetFacilityId(long targetFacilityId) {
      this.targetFacilityId = targetFacilityId;
   }

   public VehicleType getUnitTypeToCounter() {
      return unitTypeToCounter;
   }

   public void setUnitTypeToCounter(VehicleType unitTypeToCounter) {
      this.unitTypeToCounter = unitTypeToCounter;
   }
}
