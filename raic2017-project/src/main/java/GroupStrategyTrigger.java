import model.Move;

/**
 * User: goodg_000
 * Date: 23.11.2017
 * Time: 22:29
 */
public interface GroupStrategyTrigger {
   boolean isShouldApply();
   boolean apply(Move move);
}
