import model.ActionType;
import model.Game;
import model.Move;
import model.VehicleType;

import java.util.List;
import java.util.stream.Collectors;

/**
 * User: goodg_000
 * Date: 19.11.2017
 * Time: 13:51
 */
public class PackStrategy extends BattleStrategy<PackStrategyInfo> {

   private static final int MAX_ROTATIONS_COUNT = 2;
   private static final double MIN_ACCEPTABLE_DENSITY = 3.0;
   private static final double VERY_EXPANDED_DENSITY = 5.0;

   final int MIN_PACK_INTERVAL = 900;
   final int MIN_PACK_BEFORE_FIGHT_INTERVAL = 200;

   public PackStrategy(Game game, WorldInfo world, List<Group> groups, Battlefield battlefield) {
      super(game, world, groups, battlefield, false);
   }

   @Override
   public boolean apply(Move move) {

      int tick = world.getTick();
      double maxDensityFactor = Integer.MIN_VALUE;

      Group bestGroupForPack = null;
      for (Group g : getStrategyCandidatesGroups()) {

         double densityFactor = g.calcDensityFactor();

         if (!isRequirePack(densityFactor)) {
            continue;
         }

         int prevEndTick = getGroupStrategyInfo(g).getEndStrategyTick();
         boolean canDoPack = prevEndTick + MIN_PACK_INTERVAL <= tick;
         if (!canDoPack && prevEndTick + MIN_PACK_BEFORE_FIGHT_INTERVAL <= tick) {
//            List<BattleUnit> battleUnits = world.getEnemyUnits().stream().filter(
//                    u -> battlefield.isCanAttackEachOther(g.getVehicleType(), u.getType()))
//                    .collect(Collectors.toList());
//
//            List<BattleUnit> enemyUnits = Utils.filterUnitsInRange(battleUnits,
//                    g.getPos(),g.getGroupRadius()*3);
//            if (enemyUnits.size() > g.size()*0.75) {
//               Point enemyFightersPos = Utils.calcAveragePosOfUnits(enemyUnits);
//               Point mostFarFighter = enemyFightersPos.findMostFarPoint(Utils.getPositionsOfUnits(enemyUnits));
//               double groupRadius = enemyFightersPos.distanceTo(mostFarFighter);
//               double dist = g.getPos().distanceTo(enemyFightersPos);
//               if (dist < g.getGroupRadius() + groupRadius) {
//                  canDoPack = true;
//               }
//            }
         }

         if (!canDoPack) {
            continue;
         }

         if (densityFactor > maxDensityFactor) {
            bestGroupForPack = g;
            maxDensityFactor = densityFactor;
         }
      }

      if (bestGroupForPack != null) {
         final Group g = bestGroupForPack;
         return selectAndCallTrigger(bestGroupForPack, move, new SelectGroupActionStrategyTrigger() {
            @Override
            public boolean apply(Move move) {
               return doPackWithCheckIfMoved(g, move);
            }
         });
      }

      return false;
   }

   private boolean doPackWithCheckIfMoved(Group g, Move move) {
      doPack(g, move);

      getGroupStrategyInfo(g).setTrigger(new GroupStrategyTrigger() {
         @Override
         public boolean isShouldApply() {
            return g.isUnitsNotMovedSinceLastTick();
         }

         @Override
         public boolean apply(Move move) {
            if (g.isBigGroup() & g.calcDensityFactor() > VERY_EXPANDED_DENSITY &&
                    getGroupStrategyInfo(g).getRotationsCount() < MAX_ROTATIONS_COUNT && world.getTick() > 3000) {
               return selectAndCallTrigger(g, move, new SelectGroupActionStrategyTrigger() {
                  @Override
                  public boolean apply(Move move) {
                     return doRotateAndThenPack(g, move);
                  }
               });
            }
            else {
               PackStrategyInfo si = getGroupStrategyInfo(g);
               if (si.getPrevMoveTarget() != null && si.getPrevMoveTargetWithShift() != null
                       && g.getPos().distanceTo(si.getPrevMoveTargetWithShift()) > 16) {
                  return selectAndCallTrigger(g, move, new SelectGroupActionStrategyTrigger() {
                     @Override
                     public boolean apply(Move move) {
                        performMoveAction(g, si.getPrevMoveTarget(), si.getPrevMoveTargetWithShift(), move);
                        finishStrategyForGroup(g);
                        return true;
                     }
                  });
               }
               else {
                  finishStrategyForGroup(g);
                  return false;
               }
            }
         }
      });
      return true;
   }

   private boolean doRotateAndThenPack(Group g, Move move) {
      getGroupStrategyInfo(g).incRotationsCount();
      move.setAction(ActionType.ROTATE);
      move.setAngle(Math.PI);
      move.setX(g.getPos().getX());
      move.setY(g.getPos().getY());

      double maxUnitPathLength = Utils.calcArcLength(g.getGroupRadius(), move.getAngle());
      double groupSpeed = Utils.getSpeed(g.getVehicleType());
      int ticksCount = (int) (maxUnitPathLength / (groupSpeed * 2));
      final int endTick = world.getTick() + ticksCount;

      getGroupStrategyInfo(g).setTrigger(new GroupStrategyTrigger() {
         @Override
         public boolean isShouldApply() {
            return world.getTick() > endTick || g.countOfUnitsNotMovedSinceLastTick() > g.size()/2;
         }

         @Override
         public boolean apply(Move move) {
            return selectAndCallTrigger(g, move, new SelectGroupActionStrategyTrigger() {
               @Override
               public boolean apply(Move move) {
                  return doPackWithCheckIfMoved(g, move);
               }
            });
         }
      });
      return true;
   }

   private boolean isRequirePack(double densityFactor) {
      return densityFactor > MIN_ACCEPTABLE_DENSITY;
   }

   private void doPack(Group g, Move move) {
      Point target = g.getPos();
      move.setX(target.getX());
      move.setY(target.getY());
      move.setFactor(0.1);
      move.setAction(ActionType.SCALE);

      PackStrategyInfo si = getGroupStrategyInfo(g);
      if (g.getMoveTarget() != null) {
         si.setPrevMoveTarget(g.getMoveTarget());
      }
      g.setMoveTarget(null);

      if (g.getMoveTargetWithShift() != null) {
         si.setPrevMoveTargetWithShift(g.getMoveTargetWithShift());
      }
      g.setMoveTargetWithShift(null);
   }

   @Override
   public BattleStrategyType getType() {
      return BattleStrategyType.Pack;
   }

   @Override
   protected PackStrategyInfo createGroupStrategyInfo() {
      return new PackStrategyInfo();
   }
}
