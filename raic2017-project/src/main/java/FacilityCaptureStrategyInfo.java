/**
 * User: goodg_000
 * Date: 27.11.2017
 * Time: 21:22
 */
public class FacilityCaptureStrategyInfo extends GroupStrategyInfo {
   private long targetFacilityId;

   public long getTargetFacilityId() {
      return targetFacilityId;
   }

   public void setTargetFacilityId(long targetFacilityId) {
      this.targetFacilityId = targetFacilityId;
   }
}
