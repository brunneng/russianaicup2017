import model.ActionType;
import model.Game;
import model.Move;

import java.util.List;

/**
 * User: goodg_000
 * Date: 19.11.2017
 * Time: 14:32
 */
public class DoNuclearStrikeStrategy extends BattleStrategy<DoNuclearStrikeStrategyInfo> {

   public DoNuclearStrikeStrategy(Game game, WorldInfo world, List<Group> groups, Battlefield battlefield) {
      super(game, world, groups, battlefield, false);
   }

   @Override
   public boolean apply(Move move) {

      if (world.getMyPlayer().getRemainingNuclearStrikeCooldownTicks() != 0) {
         return false;
      }

      NuclearStrikeResult bestNuclearStrikeResult = null;
      for (Group g : getStrategyCandidatesGroups()) {
         NuclearStrikeResult nuclearStrikeResult = battlefield.findBestNuclearStrike(g);
         if (nuclearStrikeResult != null && (bestNuclearStrikeResult == null ||
                 nuclearStrikeResult.getScore() > bestNuclearStrikeResult.getScore())) {
            bestNuclearStrikeResult = nuclearStrikeResult;
         }
      }

      if (bestNuclearStrikeResult != null) {
         return performNuclearStrikeAction(bestNuclearStrikeResult, move);
      }

      return false;
   }

   private boolean performNuclearStrikeAction(NuclearStrikeResult nuclearStrikeResult, Move move) {
      for (Group g : nuclearStrikeResult.getMarkerUnit().findGroupsOfUnit(groups)) {
         if (isGroupBusy(g)) {
            continue;
         }

         setStrategyForGroup(g).setTrigger(new GroupStrategyTrigger() {
            @Override
            public boolean isShouldApply() {
               return world.getMyPlayer().getNextNuclearStrikeVehicleId() == -1;
            }

            @Override
            public boolean apply(Move move) {
               finishStrategyForGroup(g);
               return false;
            }
         });
      }

      move.setAction(ActionType.TACTICAL_NUCLEAR_STRIKE);
      move.setVehicleId(nuclearStrikeResult.getMarkerUnit().getId());

      Point target = nuclearStrikeResult.getTarget();
      move.setX(target.getX());
      move.setY(target.getY());
      return true;
   }

   @Override
   public BattleStrategyType getType() {
      return BattleStrategyType.DoNuclearStrike;
   }

   @Override
   protected DoNuclearStrikeStrategyInfo createGroupStrategyInfo() {
      return new DoNuclearStrikeStrategyInfo();
   }
}
