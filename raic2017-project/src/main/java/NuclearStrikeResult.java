/**
 * User: goodg_000
 * Date: 13.11.2017
 * Time: 22:04
 */
public class NuclearStrikeResult {
   private final Point target;
   private final BattleUnit markerUnit;
   private final double score;

   public NuclearStrikeResult(Point target, BattleUnit markerUnit, double score) {
      this.target = target;
      this.markerUnit = markerUnit;
      this.score = score;
   }

   public Point getTarget() {
      return target;
   }
   public BattleUnit getMarkerUnit() {
      return markerUnit;
   }
   public double getScore() {
      return score;
   }
}
