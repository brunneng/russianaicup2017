/**
 * User: goodg_000
 * Date: 23.11.2017
 * Time: 19:20
 */
public class PackStrategyInfo extends GroupStrategyInfo {
   private int rotationsCount;
   private Point prevMoveTarget;
   private Point prevMoveTargetWithShift;

   @Override
   public void restartStrategy() {
      rotationsCount = 0;
      prevMoveTarget = null;
      prevMoveTargetWithShift = null;
   }

   public void incRotationsCount() {
      rotationsCount++;
   }

   public int getRotationsCount() {
      return rotationsCount;
   }

   public Point getPrevMoveTarget() {
      return prevMoveTarget;
   }

   public void setPrevMoveTarget(Point prevMoveTarget) {
      this.prevMoveTarget = prevMoveTarget;
   }

   public Point getPrevMoveTargetWithShift() {
      return prevMoveTargetWithShift;
   }

   public void setPrevMoveTargetWithShift(Point prevMoveTargetWithShift) {
      this.prevMoveTargetWithShift = prevMoveTargetWithShift;
   }
}
