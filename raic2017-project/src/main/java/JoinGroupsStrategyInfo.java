/**
 * User: goodg_000
 * Date: 28.11.2017
 * Time: 20:25
 */
public class JoinGroupsStrategyInfo extends GroupStrategyInfo {
   private Group joinWithGroup;

   public Group getJoinWithGroup() {
      return joinWithGroup;
   }

   public void setJoinWithGroup(Group joinWithGroup) {
      this.joinWithGroup = joinWithGroup;
   }
}
