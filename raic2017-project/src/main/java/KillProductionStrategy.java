import model.*;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * User: goodg_000
 * Date: 30.11.2017
 * Time: 23:16
 */
public class KillProductionStrategy extends BattleStrategy<KillProductionStrategyInfo> {

   public KillProductionStrategy(Game game, WorldInfo world, List<Group> groups, Battlefield battlefield) {
      super(game, world, groups, battlefield, false);
   }

   @Override
   protected boolean apply(Move move) {

      Set<Long> blockedFacilities = new HashSet<>();
      for (Group g : getCurrentStrategyGroups()) {
         blockedFacilities.add(getGroupStrategyInfo(g).getTargetFacilityId());
      }

      Facility bestFacilityToBlock = null;
      Group bestBlockGroup = null;
      Point bestBlockPoint = null;
      double maxScore = Integer.MIN_VALUE;

      for (Facility f : world.getFacilities()) {
         if (!f.getType().equals(FacilityType.VEHICLE_FACTORY) ||
                 f.getOwnerPlayerId() != world.getEnemyPlayer().getId() ||
                 blockedFacilities.contains(f.getId()) ||
                 f.getVehicleType() == null) {
            continue;
         }
         long producedUnitsCount = getProducedEnemyUnitsCount(f);

         List<VehicleType> counterUnitTypes = Utils.getCounterUnitTypes(f.getVehicleType());
         for (Group g : getStrategyCandidatesGroups()) {
            if (g.isScout()) {
               continue;
            }

            boolean isCounterGroup = counterUnitTypes.contains(g.getVehicleType());
            if (!isCounterGroup && !g.getVehicleType().equals(f.getVehicleType())) {
               continue;
            }

            if (isCounterGroup) {
               if (producedUnitsCount < g.size() / 2) {
                  continue;
               }
            }
            else {
               if (producedUnitsCount > g.size()) {
                  continue;
               }
            }


            Point blockPoint = getBlockPoint(f);
            if (!battlefield.isCanSafelyMoveToPoint(g, blockPoint)) {
               continue;
            }

            double dist = g.getPos().distanceTo(blockPoint);
            double score = 10*g.size() - dist;
            if (score > maxScore) {
               maxScore = score;
               bestFacilityToBlock = f;
               bestBlockGroup = g;
               bestBlockPoint = blockPoint;
            }

         }
      }

      if (bestFacilityToBlock != null) {
         final Group g = bestBlockGroup;
         final Point target = bestBlockPoint;
         KillProductionStrategyInfo si = setStrategyForGroup(g);
         si.setTargetFacilityId(bestFacilityToBlock.getId());
         si.setUnitTypeToCounter(bestFacilityToBlock.getVehicleType());
         return selectAndCallTrigger(g, move, new SelectGroupActionStrategyTrigger() {
            @Override
            public boolean apply(Move move) {
               performMoveAction(g, target, target, move);
               getGroupStrategyInfo(g).setTrigger(new GroupStrategyTrigger() {
                  @Override
                  public boolean isShouldApply() {
                     KillProductionStrategyInfo si = getGroupStrategyInfo(g);
                     Facility f = world.getFacility(si.getTargetFacilityId());
                     long currentOwner = f.getOwnerPlayerId();
                     if (currentOwner != world.getEnemyPlayer().getId()) {
                        return true;
                     }
                     if (si.getUnitTypeToCounter() != f.getVehicleType()) {
                        return true;
                     }

                     long producedUnitsCount = getProducedEnemyUnitsCount(f);
                     if (!g.getVehicleType().equals(si.getUnitTypeToCounter())) {
                        if (producedUnitsCount < g.size() / 2) {
                           return true;
                        }
                     }

                     return !battlefield.isCanSafelyMoveToPoint(g, target);
                  }

                  @Override
                  public boolean apply(Move move) {
                     finishStrategyForGroup(g);
                     return false;
                  }
               });
               return true;
            }
         });
      }

      return false;
   }

   private long getProducedEnemyUnitsCount(Facility f) {
      return RectSelection.getFacilitySelection(f).filterUnitsInSelection(
              Utils.filterUnitsByType(world.getEnemyUnits(), f.getVehicleType())).size();
   }

   private Point getBlockPoint(Facility f) {
      double x = f.getLeft() + game.getFacilityWidth() / 2;
      double y = f.getTop() + game.getFacilityHeight();
      return new Point(x, y);
   }

   @Override
   public BattleStrategyType getType() {
      return BattleStrategyType.KillProduction;
   }

   @Override
   protected KillProductionStrategyInfo createGroupStrategyInfo() {
      return new KillProductionStrategyInfo();
   }
}
