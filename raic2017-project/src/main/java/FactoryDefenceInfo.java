import java.util.ArrayList;
import java.util.List;

/**
 * User: goodg_000
 * Date: 01.12.2017
 * Time: 22:48
 */
public class FactoryDefenceInfo {
   private List<Group> defenders = new ArrayList<>();

   public List<Group> getDefenders() {
      return defenders;
   }

   public void addDefender(Group g) {
      defenders.add(g);
   }
}
