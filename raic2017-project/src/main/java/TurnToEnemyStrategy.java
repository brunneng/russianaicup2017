import model.Game;
import model.Move;
import model.VehicleType;

import java.util.List;

/**
 * User: goodg_000
 * Date: 25.11.2017
 * Time: 17:39
 */
public class TurnToEnemyStrategy extends BattleStrategy<TurnToEnemyStrategyInfo> {

   public TurnToEnemyStrategy(Game game, WorldInfo world, List<Group> groups, Battlefield battlefield) {
      super(game, world, groups, battlefield, false);
   }

   @Override
   protected boolean apply(Move move) {

      for (Group g : getStrategyCandidatesGroups()) {
         if (g.getVehicleType().equals(VehicleType.ARRV)) {
            continue;
         }
//         if (g.isBigGroup() && !Utils.isAirUnit(g.getVehicleType())) {
//            continue;
//         }

         List<BattleUnit> enemyUnitsOfSameType = world.getEnemyUnitsOfType(g.getVehicleType());
         List<BattleUnit> closeEnemyUnits = Utils.filterUnitsInRange(enemyUnitsOfSameType, g.getPos(),
                 g.getGroupRadius() * 3);
//         final int minGroupSizeToStartRotate = 50;
//         if (closeEnemyUnits.size() < minGroupSizeToStartRotate || g.size() < minGroupSizeToStartRotate) {
//            continue;
//         }
         if (closeEnemyUnits.size() == 0 || closeEnemyUnits.size() < 5) {
            continue;
         }

         final Point centerOfEnemyGroup = Utils.calcAveragePosOfUnits(closeEnemyUnits);
         Point mostFarUnit = centerOfEnemyGroup.findMostFarPoint(Utils.getPositionsOfUnits(closeEnemyUnits));
         double enemyGroupRadius = centerOfEnemyGroup.distanceTo(mostFarUnit);
         double dist = g.getPos().distanceTo(centerOfEnemyGroup);
         if (dist > g.getGroupRadius() + enemyGroupRadius) {
            continue;
         }

         if (world.getTick() < getGroupStrategyInfo(g).getEndStrategyTick() + 100) {
            continue;
         }

         double angleToRotate = getAngleToRotate(g, new Vector2D(g.getPos(), centerOfEnemyGroup));
         if (Math.abs(angleToRotate) < Math.PI / 3) {
            continue;
         }

         return selectAndCallTrigger(g, move, new SelectGroupActionStrategyTrigger() {
            @Override
            public boolean apply(Move move) {
               double angleToRotate = getAngleToRotate(g, new Vector2D(g.getPos(), centerOfEnemyGroup));
               Point rotationCenter = g.getPos();
//               Point rotationCenter = g.calcDensityFactor() > 5.0 ? g.getPos() : g.getPos().plus(
//                       new Vector2D(centerOfEnemyGroup, g.getPos()).setLength(g.getGroupRadius()*0.8));
               performRotateAction(g, rotationCenter, angleToRotate, move);
               getGroupStrategyInfo(g).setTrigger(new GroupStrategyTrigger() {
                  @Override
                  public boolean isShouldApply() {
                     return g.isUnitsNotMovedSinceLastTick();
                  }

                  @Override
                  public boolean apply(Move move) {
                     finishStrategyForGroup(g);
                     return false;
                  }
               });

               return true;
            }
         });
      }

      return false;
   }

   @Override
   public BattleStrategyType getType() {
      return BattleStrategyType.TurnToEnemy;
   }

   @Override
   protected TurnToEnemyStrategyInfo createGroupStrategyInfo() {
      return new TurnToEnemyStrategyInfo();
   }
}
