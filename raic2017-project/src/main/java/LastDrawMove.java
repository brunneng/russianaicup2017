/**
 * User: goodg_000
 * Date: 25.11.2017
 * Time: 19:13
 */
public class LastDrawMove {

   private Point fromPos;
   private Point toPos;

   private Point rotationCenter;
   private double startAngle;
   private double rotationAngle;

   public LastDrawMove(Point fromPos, Point toPos) {
      this.fromPos = fromPos;
      this.toPos = toPos;
   }

   public LastDrawMove(Point fromPos, Point toPos, Point rotationCenter, double rotationAngle) {
      this.fromPos = fromPos;
      this.toPos = toPos;
      this.rotationCenter = rotationCenter;
      this.startAngle = new Vector2D(rotationCenter, fromPos).angle();
      this.rotationAngle = rotationAngle;
   }

   public Point getFromPos() {
      return fromPos;
   }

   public Point getToPos() {
      return toPos;
   }

   public Point getRotationCenter() {
      return rotationCenter;
   }

   public double getStartAngle() {
      return startAngle;
   }

   public double getRotationAngle() {
      return rotationAngle;
   }
}
