import model.ActionType;
import model.Game;
import model.Move;
import model.VehicleType;

import java.util.List;

/**
 * User: goodg_000
 * Date: 28.11.2017
 * Time: 20:24
 */
public class JoinGroupsStrategy extends BattleStrategy<JoinGroupsStrategyInfo> {

   private int PREFERRED_GROUND_GROUPS_COUNT = 12;
   private int PREFERRED_AIR_GROUPS_COUNT = 4;
   private int SMALL_GROUP_SIZE = 15;

   private static final double MIN_JOIN_DIST = 100;

   public JoinGroupsStrategy(Game game, WorldInfo world, List<Group> groups, Battlefield battlefield) {
      super(game, world, groups, battlefield, false);
   }

   @Override
   protected boolean apply(Move move) {

      cleanupStrategies();

      if (!getCurrentStrategyGroups().isEmpty()) {
         return false;
      }

      List<Group> strategyCandidates = getStrategyCandidatesGroups();

      Group bestGroup1ToJoin = null;
      Group bestGroup2ToJoin = null;
      double maxJoinScore = Integer.MIN_VALUE;

      for (int i = 0; i < strategyCandidates.size(); ++i) {
         Group g1 = strategyCandidates.get(i);

         for (int j = i + 1; j < strategyCandidates.size(); ++j) {
            Group g2 = strategyCandidates.get(j);

            if (!isShouldJoin(g1, g2)) {
               continue;
            }

            double dist = g1.getPos().distanceTo(g2.getPos()) - g1.getGroupRadius() - g2.getGroupRadius()
                    - BattleUnit.RADIUS*2;

            Group movedGroup = Utils.getGroupWithLessUnits(g1, g2);
            Group targetGroup = Utils.getGroupWithMoreUnits(g1, g2);

            battlefield.prepareCachesForGroup(movedGroup);
            if (battlefield.isHasCollisions(movedGroup, targetGroup, targetGroup.getMoveTargetWithShiftOrPos())) {
               continue;
            }

            double joinScore = -(g1.size() + g2.size() + dist);
            if (joinScore > maxJoinScore) {
               maxJoinScore = joinScore;
               bestGroup1ToJoin = g1;
               bestGroup2ToJoin = g2;
            }

         }
      }

      if (bestGroup1ToJoin != null) {
         return performJoin(bestGroup2ToJoin, bestGroup1ToJoin, move);
      }

      return false;
   }

   protected boolean isShouldJoin(Group g1, Group g2) {
      if (g1.isScout() || g2.isScout()) {
         return false;
      }
      if (!g1.getVehicleType().equals(g2.getVehicleType())) {
         return false;
      }

      List<Group> strategyCandidates = getStrategyCandidatesGroups();

      int groundGroupsCount = 0;
      int airGroupsCount = 0;
      for (Group g : strategyCandidates) {
         if (g.isHasAirUnits() && !g.isScout()) {
            airGroupsCount++;
         }
         if (g.isHasGroundUnits()) {
            groundGroupsCount++;
         }
      }

      boolean joinManyGroups = true;
      boolean notJoinGroundGroups = groundGroupsCount <= PREFERRED_GROUND_GROUPS_COUNT;
      boolean notJoinAirGroups = airGroupsCount <= PREFERRED_AIR_GROUPS_COUNT;

      if (g1.isHasAirUnits() && notJoinAirGroups) {
         joinManyGroups = false;
      }
      if (g1.isHasGroundUnits() && notJoinGroundGroups) {
         joinManyGroups = false;
      }
      if (g2.isHasAirUnits() && notJoinAirGroups) {
         joinManyGroups = false;
      }
      if (g2.isHasGroundUnits() && notJoinGroundGroups) {
         joinManyGroups = false;
      }

      boolean joinSmallGroups = g1.size() <= SMALL_GROUP_SIZE || g2.size() <= SMALL_GROUP_SIZE;
      if (!joinManyGroups && !joinSmallGroups) {
         return false;
      }

      double dist = g1.getPos().distanceTo(g2.getPos()) - g1.getGroupRadius() - g2.getGroupRadius()
              - BattleUnit.RADIUS*2;
      if (dist > MIN_JOIN_DIST) {
         return false;
      }

      return true;
   }

   protected void cleanupStrategies() {
      for (Group g : getCurrentStrategyGroups()) {
         JoinGroupsStrategyInfo si = getGroupStrategyInfo(g);
         Group otherGroup = si.getJoinWithGroup();
         if (si.getTrigger() == null && (!isCurrentStrategy(otherGroup) || otherGroup.isEliminated())) {
            finishStrategyForGroup(g);
         }
      }
   }

   protected boolean performJoin(Group bestGroup2ToJoin, Group bestGroup1ToJoin, Move move) {
      boolean sameSize = bestGroup1ToJoin.size() == bestGroup2ToJoin.size();

      final Group movedGroup = sameSize ?
              bestGroup1ToJoin : Utils.getGroupWithLessUnits(bestGroup1ToJoin, bestGroup2ToJoin);

      final Group targetGroup = sameSize ?
              bestGroup2ToJoin : Utils.getGroupWithMoreUnits(bestGroup1ToJoin, bestGroup2ToJoin);

      setStrategyForGroup(movedGroup).setJoinWithGroup(targetGroup);
      setStrategyForGroup(targetGroup).setJoinWithGroup(movedGroup);
      return selectAndCallTrigger(movedGroup, move, new SelectGroupActionStrategyTrigger() {
         @Override
         public boolean apply(Move move) {
            if (isOtherGroupNotReadyForJoin(movedGroup, targetGroup)) {
               return false;
            }

            Point target = targetGroup.getMoveTargetWithShiftOrPos();
            performMoveAction(movedGroup, target, target, move);
            int moveTime = (int) (movedGroup.getPos().distanceTo(target) /
                    Utils.getSpeed(movedGroup.getVehicleType()));
            final int finishMoveTick = world.getTick() + moveTime;
            getGroupStrategyInfo(movedGroup).setTrigger(new GroupStrategyTrigger() {
               @Override
               public boolean isShouldApply() {
                  return world.getTick() > finishMoveTick;
               }

               @Override
               public boolean apply(Move move) {
                  return selectAndCallTrigger(movedGroup, move, new SelectGroupActionStrategyTrigger() {
                     @Override
                     public boolean apply(Move move) {
                        if (isOtherGroupNotReadyForJoin(movedGroup, targetGroup)) {
                           return false;
                        }

                        addGroupToSelection(targetGroup, move);
                        getGroupStrategyInfo(targetGroup).setTrigger(new GroupStrategyTrigger() {
                           @Override
                           public boolean isShouldApply() {
                              return true;
                           }

                           @Override
                           public boolean apply(Move move) {
                              if (isOtherGroupNotReadyForJoin(movedGroup, targetGroup)) {
                                 return false;
                              }
                              if (!targetGroup.isSelected() || !movedGroup.isSelected()) {
                                 finishStrategyForGroup(targetGroup);
                                 return false;
                              }

                              return assignGroup(movedGroup, targetGroup, move);
                           }
                        });
                        return true;
                     }
                  });
               }
            });

            return true;
         }
      });
   }

   private boolean isOtherGroupNotReadyForJoin(Group g1, Group g2) {
      if (isCurrentStrategy(g1) && isCurrentStrategy(g2) && getGroupStrategyInfo(g1).getJoinWithGroup() == g2 &&
              getGroupStrategyInfo(g2).getJoinWithGroup() == g1) {
         return false;
      }

      finishStrategyForGroup(g1);
      return true;
   }

   private boolean assignGroup(Group g1, Group g2, Move move) {
      move.setAction(ActionType.ASSIGN);
      int nextGroupId = CreateGroupsStrategy.getNextGroupId();
      move.setGroup(nextGroupId);
      Group g = new Group(nextGroupId);
      g.addUnits(g1.getUnits());
      g.addUnits(g2.getUnits());
      g.setVehicleType(Utils.getVehicleTypeWithMaxUnits(g.getUnits()));
      g.setSelected(true);
      g.update();
      groups.remove(g1);
      groups.remove(g2);
      CreateGroupsStrategy.returnGroupId(g1.getId());
      CreateGroupsStrategy.returnGroupId(g2.getId());
      groups.add(g);
      return true;
   }

   @Override
   public BattleStrategyType getType() {
      return BattleStrategyType.JoinGroups;
   }

   @Override
   protected JoinGroupsStrategyInfo createGroupStrategyInfo() {
      return new JoinGroupsStrategyInfo();
   }
}
