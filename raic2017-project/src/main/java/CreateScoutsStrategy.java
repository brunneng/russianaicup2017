import model.ActionType;
import model.Game;
import model.Move;
import model.VehicleType;

import java.util.Arrays;
import java.util.List;

/**
 * User: goodg_000
 * Date: 08.12.2017
 * Time: 18:46
 */
public class CreateScoutsStrategy extends BattleStrategy<GroupStrategyInfo> {

   private Group fightersGroup;
   private static BattleUnit selectedFighter;
   private long selectionTick;
   private boolean groupRemovedFromFighter;
   private boolean movedOut;

   public CreateScoutsStrategy(Game game, WorldInfo world, List<Group> groups, Battlefield battlefield) {
      super(game, world, groups, battlefield, false);
   }

   @Override
   protected boolean apply(Move move) {

      if (selectedFighter != null && !selectedFighter.isRemoved()) {
         if (selectionTick != Army.getLastSelectTick()) {
            return selectUnit(selectedFighter, move);
         }

         if (!groupRemovedFromFighter) {
            return removeFromGroup(move);
         }
         else if (!movedOut) {
            return goOutFromGroup(move);
         }
         else {
            return createScoutGroup(move);
         }
      }

      selectedFighter = null;
      fightersGroup = null;
      groupRemovedFromFighter = false;
      movedOut = false;
      int scoutsCount = 0;
      for (Group g : groups) {
         if (g.isScout()) {
            scoutsCount++;
         }
         else if (g.getVehicleType().equals(VehicleType.FIGHTER) && g.size() > 1) {
            fightersGroup = g;
         }
      }

      if (!isRequireNewScout(scoutsCount)) {
         return false;
      }

      if (fightersGroup == null) {
         return false;
      }

      Point nearestEnemy = fightersGroup.getPos().findNearestPoint(Utils.getPositionsOfUnits(world.getEnemyUnits()));
      double enemyCheckRadius = Math.max(fightersGroup.getGroupRadius()*3, 100);
      if (nearestEnemy != null && fightersGroup.getPos().distanceTo(nearestEnemy) < enemyCheckRadius) {
         return false;
      }

      Vector2D averageVelocity = Utils.calcAverageVelocity(fightersGroup.getUnits());
      if (!averageVelocity.isZero() &&
              Math.abs(averageVelocity.angleToVector(new Vector2D(1, 0))) < Math.PI / 9) {
         return false;
      }

      setStrategyForGroup(fightersGroup);
      BattleUnit bomber = findRightmostFighter(fightersGroup);
      return selectUnit(bomber, move);
   }

   private boolean isRequireNewScout(int scoutsCount) {
      int requiredScouts = 1;
      if (game.isFogOfWarEnabled()) {
         int tick = world.getTick();
         if (tick > 1500 && tick <= 3000) {
            requiredScouts = 2;
         }
         if (tick > 3000) {
            requiredScouts = 2;
         }
      }
      return scoutsCount < requiredScouts;
   }


   private boolean createScoutGroup(Move move) {
      move.setAction(ActionType.ASSIGN);
      move.setGroup(CreateGroupsStrategy.getNextGroupId());
      finishStrategyForGroup(fightersGroup);

      Group scoutGroup = new Group(move.getGroup());
      scoutGroup.setScout(true);
      scoutGroup.addUnits(Arrays.asList(selectedFighter));
      groups.add(scoutGroup);
      fightersGroup.getUnits().remove(selectedFighter);

      selectedFighter = null;
      return true;
   }

   private boolean goOutFromGroup(Move move) {
      Vector2D moveVector = new Vector2D(fightersGroup.getPos(), selectedFighter.getPos());
      moveVector.setLength(20);
      move.setX(moveVector.getX());
      move.setY(moveVector.getY());
      move.setAction(ActionType.MOVE);
      movedOut = true;
      return true;
   }

   private boolean selectUnit(BattleUnit bomber, Move move) {
      for (Group g : groups) {
         g.setSelected(false);
      }

      move.setAction(ActionType.CLEAR_AND_SELECT);
      RectSelection.getSelectionForUnits(Arrays.asList(bomber)).updateMove(move);
      selectedFighter = bomber;
      selectionTick = world.getTick();
      return true;
   }

   private boolean removeFromGroup(Move move) {
      move.setAction(ActionType.DISMISS);
      move.setGroup(fightersGroup.getId());
      groupRemovedFromFighter = true;
      return true;
   }

   private BattleUnit findRightmostFighter(Group g) {
      double maxX = 0;
      BattleUnit res = null;
      for (BattleUnit u : g.getUnits()) {
         if (u.getType().equals(VehicleType.FIGHTER)) {
            double x = u.getPos().getX();
            if (x > maxX) {
               res = u;
               maxX = x;
            }
         }
      }
      return res;
   }

   @Override
   public BattleStrategyType getType() {
      return BattleStrategyType.CreateScouts;
   }

   @Override
   protected GroupStrategyInfo createGroupStrategyInfo() {
      return new GroupStrategyInfo();
   }

   public static boolean isFutureScout(BattleUnit u) {
      return selectedFighter == u;
   }
}
