import model.*;

import java.util.*;

/**
 * User: goodg_000
 * Date: 27.11.2017
 * Time: 21:22
 */
public class FacilityCaptureStrategy extends BattleStrategy<FacilityCaptureStrategyInfo> {

   private static class CapturePoint {
      private final Point point;
      private final boolean forBigGroup;

      public CapturePoint(Point point, boolean forBigGroup) {
         this.point = point;
         this.forBigGroup = forBigGroup;
      }

      public Point getPoint() {
         return point;
      }

      public boolean isForBigGroup() {
         return forBigGroup;
      }
   }

   private class CaptureInfo implements Comparable<CaptureInfo> {
      private final Group group;
      private final Facility facility;
      private final double score;
      private final Point capturePos;

      public CaptureInfo(Group group, Facility facility, double score, Point capturePos) {
         this.group = group;
         this.facility = facility;
         this.score = score;
         this.capturePos = capturePos;
      }

      public Group getGroup() {
         return group;
      }

      public Facility getFacility() {
         return facility;
      }

      public Point getCapturePos() {
         return capturePos;
      }

      @Override
      public int compareTo(CaptureInfo o) {
         return -Double.compare(score, o.score);
      }
   }

   public FacilityCaptureStrategy(Game game, WorldInfo world, List<Group> groups, Battlefield battlefield) {
      super(game, world, groups, battlefield, false);
   }

   @Override
   protected boolean apply(Move move) {

      Map<Long, Group> facilityIdToCaptureGroup = new HashMap<>();
      for (Group g : getCurrentStrategyGroups()) {
         FacilityCaptureStrategyInfo si = getGroupStrategyInfo(g);
         facilityIdToCaptureGroup.put(si.getTargetFacilityId(), g);
      }

      List<CaptureInfo> captureInfos = new ArrayList<>();

      for (Facility f : world.getFacilities()) {
         if (world.isMyFacility(f)) {
            continue;
         }

         List<Group> groups = new ArrayList<>(getStrategyCandidatesGroups());
         groups.addAll(getCurrentStrategyGroups());
         for (Group g : groups) {
            if (!g.isHasGroundUnits()) {
               continue;
            }

            Point capturePos = findCapturePos(f, g);
            if (!battlefield.isCanSafelyMoveToPoint(g, capturePos)) {
               continue;
            }

            double dist = g.getPos().distanceTo(capturePos);
            double captureScore = 4*g.size() - dist;
            if (f.getType().equals(FacilityType.VEHICLE_FACTORY)) {
               captureScore += 300;
               if (!g.getVehicleType().equals(VehicleType.ARRV)) {
                  captureScore += 2500;
               }
            }

            captureInfos.add(new CaptureInfo(g, f, captureScore, capturePos));
         }

      }

      Collections.sort(captureInfos);
      Set<Long> excludedFacilitiesIds = new HashSet<>();
      Set<Integer> excludedGroupIds = new HashSet<>();
      for (CaptureInfo ci : captureInfos) {
         final Group g = ci.getGroup();
         final Facility f = ci.getFacility();

         if (excludedFacilitiesIds.contains(f.getId()) || excludedGroupIds.contains(g.getId())) {
            continue;
         }

         if (isCurrentStrategy(g) && getGroupStrategyInfo(g).getTargetFacilityId() == f.getId()) {
            excludedFacilitiesIds.add(ci.getFacility().getId());
            excludedGroupIds.add(g.getId());
            continue;
         }

         Group prevGroup = facilityIdToCaptureGroup.get(f.getId());
         if (prevGroup != null && g != prevGroup) {
            finishStrategyForGroup(prevGroup);
         }

         final Point target = ci.getCapturePos();
         setStrategyForGroup(g).setTargetFacilityId(f.getId());
         return selectAndCallTrigger(g, move, new SelectGroupActionStrategyTrigger() {
            @Override
            public boolean apply(Move move) {
               performMoveAction(g, target, target, move);
               getGroupStrategyInfo(g).setTrigger(new GroupStrategyTrigger() {
                  @Override
                  public boolean isShouldApply() {
                     long targetFacilityId = getGroupStrategyInfo(g).getTargetFacilityId();
                     Facility cf = world.getFacility(targetFacilityId);
                     if (cf.getOwnerPlayerId() == world.getMyPlayerId()) {
                        return true;
                     }

                     if (cf.getOwnerPlayerId() == WorldInfo.NEUTRAL_PLAYER_ID ||
                             RectSelection.getFacilitySelection(cf).isInSelection(g.getPos())) {
                        battlefield.prepareCachesForGroup(g);

                        if (battlefield.isGroupInDanger(g)) {
                           Point dangerPos = findNearestDangerCellPos(g);
                           if (dangerPos != null) {
                              double dist = g.getPos().distanceTo(dangerPos);
                              double ticksToComeAndKill = dist / Utils.getSpeed(g.getVehicleType());
                              List<BattleUnit> nearEnemyUnits = Utils.filterUnitsInRange(
                                      world.getEnemyUnits(), g.getPos(), g.getGroupRadius() * 3);
                              if (!nearEnemyUnits.isEmpty()) {
                                 Integer ticksToDefend = battlefield.getMaxTicksToDefend(g.getUnits(), nearEnemyUnits);
                                 if (ticksToDefend != null) {
                                    ticksToComeAndKill += ticksToDefend;
                                 }
                              }

                              int ticksToCapture = battlefield.getTicksToCapture(cf, true);
                              if (ticksToCapture >= 0 && ticksToCapture > ticksToComeAndKill) {
                                 return true;
                              }
                           }
                        }

                        return battlefield.isHasCollisions(g, g.getMoveTargetWithShift()) ||
                                battlefield.calcBattleScoreOnPath(g, g.getMoveTargetWithShift()) < 0;
                     }

                     return !battlefield.isCanSafelyMoveToPoint(g, g.getMoveTargetWithShiftOrPos());
                  }

                  @Override
                  public boolean apply(Move move) {
                     finishStrategyForGroup(g);
                     return false;
                  }
               });
               return true;
            }
         });
      }

      return false;
   }

   private Point findCapturePos(Facility f, Group g) {
      double w = game.getFacilityWidth();
      double h = game.getFacilityHeight();

      RectSelection rect = RectSelection.getSelectionForUnits(g.getUnits());
      if (rect.getWidth() < w && rect.getHeight() < h) {
         double dx = (f.getLeft() + w) - rect.getToX() - BattleUnit.RADIUS;
         double dy = (f.getTop() + h) - rect.getToY() - 3*BattleUnit.RADIUS;
         return g.getPos().plus(new Vector2D(dx, dy));
      }
      else {
         return battlefield.getFacilityCenter(f);
      }
   }

   private Point findNearestDangerCellPos(Group g) {
      List<CellInfo> dangerCells = battlefield.calcDangerCells(g);
      Point pos = g.getPos();
      return pos.findNearestPoint(Utils.getPositionsOfCells(dangerCells));
   }

   @Override
   public BattleStrategyType getType() {
      return BattleStrategyType.FacilityCapture;
   }

   @Override
   protected FacilityCaptureStrategyInfo createGroupStrategyInfo() {
      return new FacilityCaptureStrategyInfo();
   }
}
