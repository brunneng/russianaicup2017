import model.*;

import java.util.*;
import java.util.stream.Collectors;

/**
 * User: goodg_000
 * Date: 26.11.2017
 * Time: 14:39
 */
public class CreateGroupsStrategy extends BattleStrategy<CreateGroupStrategyInfo> {

   private final int MIN_EMERGENCY_GROUP_SIZE = 11;

   private static LinkedList<Integer> freeGroupIds = new LinkedList<>();
   private LinkedList<CreateGroupRequest> createGroupRequests = new LinkedList<>();

   public CreateGroupsStrategy(Game game, WorldInfo world, List<Group> groups, Battlefield battlefield) {
      super(game, world, groups, battlefield,false);

      for (int groupId = 1; groupId <= game.getMaxUnitGroup(); ++groupId) {
         freeGroupIds.add(groupId);
      }
   }

   @Override
   protected boolean apply(Move move) {
      if (!createGroupRequests.isEmpty()) {
         return processCreationOfGroups(move);
      }

      for (Facility f : world.getFacilities()) {
         if (!f.getType().equals(FacilityType.VEHICLE_FACTORY)) {
            continue;
         }

         FactoryProductionInfo productionInfo = FactoryProductionStrategy.getProductionInfo(f);
         if (productionInfo == null) {
            continue;
         }

         List<BattleUnit> freeUnits = battlefield.getProducedUnits(f);

         if (isIntersectWithOtherGroup(freeUnits)) {
            return requestCreationOfGroup(f, freeUnits, move);
         }

         if (freeUnits.size() < MIN_EMERGENCY_GROUP_SIZE) {
            continue;
         }

         if (isCapturedByEnemyAndCantDefend(f)) {
            return requestCreationOfGroup(f, freeUnits, move);
         }

         if (f.getVehicleType() == VehicleType.TANK && FactoryDefenceStrategy.isRequireDefence(f, battlefield)) {
            continue;
         }

         if (isEnoughUnitsToCreateGroup(freeUnits)) {
            return requestCreationOfGroup(f, freeUnits, move);
         }

         Point centerOfFreeUnits = Utils.calcAveragePosOfUnits(freeUnits);
         List<BattleUnit> enemyUnits = Utils.filterUnitsInRange(world.getEnemyUnits(), centerOfFreeUnits, 96);
         if (enemyUnits.isEmpty()) {
            continue;
         }

         VehicleType freeUnitsType = Utils.getVehicleTypeWithMaxUnits(freeUnits);
         VehicleType enemyUnitsVehicleType = Utils.getVehicleTypeWithMaxUnits(enemyUnits);
         if (Utils.getCounterUnitTypes(freeUnitsType).contains(enemyUnitsVehicleType)) {
            return requestCreationOfGroup(f, freeUnits, move);
         }
      }
      return false;
   }

   public boolean isCapturedByEnemyAndCantDefend(Facility f) {
      RectSelection rect = RectSelection.getFacilitySelection(f);

      List<BattleUnit> myUnits = rect.filterUnitsInSelection(Army.world.getMyUnits()).stream().filter(u ->
              !Utils.isAirUnit(u.getType())).collect(Collectors.toList());
      List<BattleUnit> enemyUnits = rect.filterUnitsInSelection(Army.world.getEnemyUnits()).stream().filter(u ->
              !Utils.isAirUnit(u.getType())).collect(Collectors.toList());

      if (enemyUnits.isEmpty()) {
         return false;
      }

      return !battlefield.isCanDefend(myUnits, f.getVehicleType(), enemyUnits);
   }

   private boolean isIntersectWithOtherGroup(List<BattleUnit> freeUnits) {
      boolean hasGroundUnits = Utils.filterGroundUnits(freeUnits).size() > 0;
      boolean hasAirUnits = Utils.filterAirUnits(freeUnits).size() > 0;

      List<BattleUnit> allUnitsInSelection = RectSelection.getSelectionForUnits(freeUnits).filterUnitsInSelection(
              world.getMyUnits());
      for (BattleUnit u : allUnitsInSelection) {
         if (!u.isWithoutGroup()) {
            if ((!u.isAerial() && hasGroundUnits) || (u.isAerial() && hasAirUnits)) {
               return true;
            }
         }
      }
      return false;
   }

   private boolean isEnoughUnitsToCreateGroup(List<BattleUnit> freeUnits) {
      double points = 0;
      for (BattleUnit u : freeUnits) {
         if (u.isAerial()) {
            points += 1.5;
         }
         else {
            points += 1;
         }
      }

      return points >= 32;
   }

   private VehicleType getSingleVehicleType(List<BattleUnit> battleUnits) {
      VehicleType res = null;
      for (BattleUnit u : battleUnits) {
         if (res == null) {
            res = u.getType();
         }
         else if (!res.equals(u.getType())) {
            res = null;
            break;
         }
      }
      return res;
   }

   private boolean processCreationOfGroups(Move move) {
      final CreateGroupRequest r = createGroupRequests.getFirst();
      if (r.getGroupId() == null) {
         r.setGroupId(getNextGroupId());
      }

      if (!r.isSelected()) {
         for (Group g : groups) {
            if (g.isSelected()) {
               g.setSelected(false);
            }
         }

         RectSelection s = RectSelection.getSelectionForUnits(r.getUnitsToSelect());
         VehicleType singleVehicleType = getSingleVehicleType(r.getUnitsToSelect());

         List<Group> consumedGroups = new ArrayList<>();
         boolean finish = false;
         while (!finish) {
            List<BattleUnit> allUnitsInSelection = s.filterUnitsInSelection(world.getMyUnits());
            if (singleVehicleType != null) {
               for (BattleUnit u : allUnitsInSelection) {
                  if (Utils.isAirUnit(singleVehicleType)) {
                     if (u.isAerial() && !u.getType().equals(singleVehicleType)) {
                        singleVehicleType = null;
                        break;
                     }
                  }
                  else {
                     if (!u.isAerial() && !u.getType().equals(singleVehicleType)) {
                        singleVehicleType = null;
                        break;
                     }
                  }
               }
            }
            if (singleVehicleType != null) {
               final VehicleType t = singleVehicleType;
               allUnitsInSelection = allUnitsInSelection.stream().filter(u -> u.getType().equals(t)).collect(
                       Collectors.toList());
            }

            finish = true;
            for (BattleUnit u : allUnitsInSelection) {
               if (!u.isWithoutGroup()) {
                  List<Group> unitGroups = u.getGroups(groups);
                  for (Group g : unitGroups) {
                     if (!consumedGroups.contains(g)) {
                        finish = false;
                        consumedGroups.add(g);
                     }
                  }
               }
            }

            if (!finish) {
               Set<BattleUnit> extendedUnits = new HashSet<>(allUnitsInSelection);
               for (Group g : consumedGroups) {
                  extendedUnits.addAll(g.getUnits());
               }
               r.setUnitsToSelect(new ArrayList<>(extendedUnits));
               s = RectSelection.getSelectionForUnits(extendedUnits);
            }
         }
         for (Group g : consumedGroups) {
            groups.remove(g);
            returnGroupId(g.getId());
         }

         move.setAction(ActionType.CLEAR_AND_SELECT);
         move.setVehicleType(singleVehicleType);
         s.updateMove(move);
         r.setSelected();
      }
      else if (r.getGroup() == null) {
         move.setAction(ActionType.ASSIGN);
         move.setGroup(r.getGroupId());
         move.setVehicleType(r.getVehicleType());
         Group g = new Group(r.getGroupId());
         g.addUnits(r.getUnitsToSelect());
         g.setVehicleType(r.getVehicleType());
         g.setSelected(true);
         r.setGroup(g);
         g.update();
         groups.add(g);

         setStrategyForGroup(g).setTrigger(new SelectGroupActionStrategyTrigger() {
            @Override
            public boolean apply(Move move) {
               if (r.getProducedFacility() != null) {
                  Point target = findBestMoveTarget(g, r.getProducedFacility());
                  performMoveAction(g, target, target, move);
                  finishStrategyForGroup(g);
                  return true;
               }
               else {
                  doPack(g, move);
                  finishStrategyForGroup(g);
               }

               return true;
            }
         });

         createGroupRequests.removeFirst();
      }
      return true;
   }

   private Point findBestMoveTarget(Group g, Facility producedFacility) {
      if (Utils.isCapturedByEnemy(producedFacility)) {
         if (!battlefield.isGroupInDanger(g)) {
            return battlefield.getFacilityCenter(producedFacility);
         }
      }

      double moveDist = game.getFacilityWidth() * 2;
      Point centerOfField = new Point(world.getWidth() / 2, world.getHeight() / 2);
      double startAngle = new Vector2D(g.getPos(), centerOfField).angle();

      int stepsCount = 18;
      double angleStep = 2 * Math.PI / stepsCount;

      battlefield.prepareCachesForGroup(g);
      Point res = centerOfField;
      double maxScore = Integer.MIN_VALUE;

      for (int i = 0; i < stepsCount; ++i) {
         double angle = startAngle + i * angleStep;
         Point target = g.getPos().plus(Vector2D.fromAngleAndLength(angle, moveDist));
         target = Utils.ensureInBoundsOfField(target);
         if (!battlefield.isHasCollisions(g, target)) {
            double score = battlefield.calcBattleScoreOnPath(g, target);
            if (score > maxScore) {
               res = target;
               maxScore = score;
            }
         }
      }

      return res;
   }

   private boolean doPack(Group g, Move move) {
      Point target = g.getPos();
      move.setX(target.getX());
      move.setY(target.getY());
      move.setFactor(0.1);
      move.setAction(ActionType.SCALE);
      PackStrategyInfo packStrategyInfo = new PackStrategyInfo();
      packStrategyInfo.restartStrategy();
      packStrategyInfo.setEndStrategyTick(world.getTick());
      g.setGroupStrategyInfo(BattleStrategyType.Pack, packStrategyInfo);
      return true;
   }

   public boolean requestCreationOfGroup(Facility producedFacility, List<BattleUnit> units, Move move) {
      VehicleType vehicleType = Utils.getVehicleTypeWithMaxUnits(units);
      CreateGroupRequest request = new CreateGroupRequest(units, vehicleType);
      request.setProducedFacility(producedFacility);
      createGroupRequests.add(request);
      return processCreationOfGroups(move);
   }

   public void addCreateGroupRequests(List<CreateGroupRequest> requests) {
      createGroupRequests.addAll(requests);
   }

   @Override
   public BattleStrategyType getType() {
      return BattleStrategyType.CreateGroups;
   }

   @Override
   protected CreateGroupStrategyInfo createGroupStrategyInfo() {
      return new CreateGroupStrategyInfo();
   }

   public static int getNextGroupId() {
      int res = freeGroupIds.pollFirst();
      return res;
   }

   public static void returnGroupId(int groupId) {
      freeGroupIds.add(groupId);
   }
}
