import model.Game;
import model.Move;

import java.util.List;

/**
 * User: goodg_000
 * Date: 30.11.2017
 * Time: 20:55
 */
public class JoinCollidedGroupsStrategy extends JoinGroupsStrategy {

   public JoinCollidedGroupsStrategy(Game game, WorldInfo world, List<Group> groups, Battlefield battlefield) {
      super(game, world, groups, battlefield);
   }

   @Override
   protected boolean apply(Move move) {
      cleanupStrategies();

      List<Group> strategyCandidates = getStrategyCandidatesGroups();
      for (int i = 0; i < strategyCandidates.size(); ++i) {
         for (int j = i + 1; j < strategyCandidates.size(); ++j) {
            Group g1 = strategyCandidates.get(i);
            Group g2 = strategyCandidates.get(j);
            if (g1.isScout() || g2.isScout()) {
               continue;
            }

            if (isCollided(g1, g2) && g1.isUnitsNotMovedSinceLastTick() && g2.isUnitsNotMovedSinceLastTick()) {
               return performJoin(g1, g2, move);
            }
         }
      }

      return false;
   }

   private boolean isCollided(Group g1, Group g2) {
      if (!g1.isCanCollideWithGroup(g2)) {
         return false;
      }

      double dist = g1.getPos().distanceTo(g2.getPos());
      return dist < g1.getGroupRadius() + g2.getGroupRadius() + 2 * BattleUnit.RADIUS;
   }

   @Override
   public BattleStrategyType getType() {
      return BattleStrategyType.JoinCollidedGroups;
   }
}
