import model.Facility;
import model.VehicleType;

import java.util.List;
import java.util.stream.Collectors;

/**
 * User: goodg_000
 * Date: 26.11.2017
 * Time: 17:43
 */
public class FactoryProductionInfo {
   private final int productionStartTick;

   public FactoryProductionInfo(int productionStartTick, VehicleType producedVehicleType) {
      this.productionStartTick = productionStartTick;
   }

   public int getProductionStartTick() {
      return productionStartTick;
   }
}
