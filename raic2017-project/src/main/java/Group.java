import model.VehicleType;

import java.util.ArrayList;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * User: goodg_000
 * Date: 09.11.2017
 * Time: 19:07
 */
public class Group {
   private final int id;
   private double totalLostDurability;
   private UnitsCounts unitsCounts = new UnitsCounts();
   private List<BattleUnit> units = new ArrayList<>();
   private VehicleType vehicleType;
   private boolean hasGroundUnits;
   private boolean hasAirUnits;
   private double groupRadius;
   private double pointsAxis;

   private Point pos;
   private Point averagePosOfDamagedUnits;

   private Point moveTarget;
   private Point moveTargetWithShift;

   private double totalDurability;

   private boolean selected;
   private boolean scout;

   private BattleStrategyType currentStrategyType;
   private Map<BattleStrategyType, GroupStrategyInfo> groupStrategyInfos = new EnumMap<>(BattleStrategyType.class);

   public Group(int id) {
      this.id = id;
   }

   public void addUnits(List<BattleUnit> battleUnits) {
      units.addAll(battleUnits);
   }

   public void setVehicleType(VehicleType vehicleType) {
      this.vehicleType = vehicleType;
   }

   public void update() {
      units.removeIf(BattleUnit::isRemoved);
      if (!isEliminated()) {
         updateAveragePos();
         updateAveragePosOfDamagedUnits();
         updateTotalLostDurability();
         updateUnitsInfo();
         updateGroupRadius();
         updateTotalDurability();
         updateVehicleType();
      }
      Draw.group(this);
   }

   private void updateVehicleType() {
      setVehicleType(Utils.getVehicleTypeWithMaxUnits(units));
      hasGroundUnits = false;
      hasAirUnits = false;
      for (BattleUnit u : units) {
         if (u.isAerial()) {
            hasAirUnits = true;
         }
         else {
            hasGroundUnits = true;
         }
      }
   }

   public void updateBeforeMove() {
      updatePointsAxis();
   }

   private void updateTotalDurability() {
      totalDurability = 0;
      for (BattleUnit u : units) {
         totalDurability += u.getDurability();
      }
   }

   private void updateUnitsInfo() {
      unitsCounts.clear();
      for (BattleUnit u : units) {
         unitsCounts.add(u.getType(), u.getDurabilityPercent());
      }
   }

   private void updateGroupRadius() {
      Point mostFarPoint = pos.findMostFarPoint(Utils.getPositionsOfUnits(units));
      groupRadius = pos.distanceTo(mostFarPoint);
   }

   private void updatePointsAxis() {
      pointsAxis = Utils.calcPointsAxis(Utils.getPositionsOfUnits(units), pos,
              groupRadius, pointsAxis);
   }

   public boolean isEliminated() {
      return units.isEmpty();
   }

   private void updateAveragePos() {
      pos = Utils.calcAveragePosOfUnits(units);
   }

   private void updateAveragePosOfDamagedUnits() {
      List<BattleUnit> damagedUnits = units.stream().filter(u -> u.getLostDurability() > 0).collect(
              Collectors.toList());

      if (damagedUnits.size() > 0) {
         averagePosOfDamagedUnits = Utils.calcAveragePosOfUnits(damagedUnits);
      }
      else {
         averagePosOfDamagedUnits = pos;
      }
   }

   private void updateTotalLostDurability() {
      totalLostDurability = 0;
      for (BattleUnit u : units) {
         totalLostDurability += u.getLostDurability();
      }
   }

   public boolean isUnitsNotMovedSinceLastTick() {
      for (BattleUnit u : units) {
         if (u.getVelocity().length() > Utils.SMALL_DOUBLE) {
            return false;
         }
      }

      return true;
   }

   public int countOfUnitsNotMovedSinceLastTick() {
      int res = 0;
      for (BattleUnit u : units) {
         if (u.getVelocity().length() <= Utils.SMALL_DOUBLE) {
            res++;
         }
      }

      return res;
   }

   public int getId() {
      return id;
   }

   public VehicleType getVehicleType() {
      return vehicleType;
   }

   public boolean isHasAirUnits() {
      return hasAirUnits;
   }

   public boolean isHasGroundUnits() {
      return hasGroundUnits;
   }

   public boolean isCanCollideWithGroup(Group g) {
      return (isHasAirUnits() && g.isHasAirUnits()) || (isHasGroundUnits() && g.isHasGroundUnits());
   }

   public Point getPos() {
      return pos;
   }

   public Point getMoveTarget() {
      return moveTarget;
   }

   public void setMoveTarget(Point moveTarget) {
      this.moveTarget = moveTarget;
   }

   public int size() {
      return units.size();
   }

   public List<BattleUnit> getUnits() {
      return units;
   }

   public UnitsCounts getUnitsCounts() {
      return unitsCounts;
   }

   public double getTotalLostDurability() {
      return totalLostDurability;
   }

   public double getTotalLostDurabilityPercent() {
      return totalLostDurability / (size() * BattleUnit.MAX_DURABILITY);
   }

   public double getGroupRadius() {
      return groupRadius;
   }

   public Point getAveragePosOfDamagedUnits() {
      return averagePosOfDamagedUnits;
   }

   public BattleStrategyType getCurrentStrategyType() {
      return currentStrategyType;
   }

   public void setCurrentStrategyType(BattleStrategyType currentStrategyType) {
      this.currentStrategyType = currentStrategyType;
   }

   public boolean isBigGroup() {
      return size() >= 50;
   }

   public boolean isSelected() {
      return selected;
   }

   public void setSelected(boolean selected) {
      this.selected = selected;
      if (!selected) {
         for (BattleStrategyType t : BattleStrategyType.values()) {
            GroupStrategyInfo strategyInfo = getGroupStrategyInfo(t);
            if (strategyInfo != null && strategyInfo.getTrigger() instanceof SelectGroupActionStrategyTrigger) {
               strategyInfo.setTrigger(null);
               if (getCurrentStrategyType() == t) {
                  setCurrentStrategyType(null);
               }
            }
         }
      }
   }

   public Point getMoveTargetWithShiftOrPos() {
      return moveTargetWithShift != null ? moveTargetWithShift : pos;
   }

   public Point getMoveTargetWithShift() {
      return moveTargetWithShift;
   }

   public void setMoveTargetWithShift(Point moveTargetWithShift) {
      this.moveTargetWithShift = moveTargetWithShift;
   }

   public double getTotalDurability() {
      return totalDurability;
   }

   public GroupStrategyInfo getGroupStrategyInfo(BattleStrategyType battleStrategyType) {
      return groupStrategyInfos.get(battleStrategyType);
   }

   public void setGroupStrategyInfo(BattleStrategyType battleStrategyType, GroupStrategyInfo groupStrategyInfo) {
      groupStrategyInfos.put(battleStrategyType, groupStrategyInfo);
   }

   public double getPointsAxis() {
      return pointsAxis;
   }

   boolean isScout() {
      return scout;
   }

   public void setScout(boolean scout) {
      this.scout = scout;
   }

   public double calcDensityFactor() {
      double square = Utils.calcCircleSquare(getGroupRadius());
      double unitSquare = Utils.calcCircleSquare(BattleUnit.RADIUS);
      double totalUnitsSquare = size()*unitSquare;
      return square / totalUnitsSquare;
   }
}
