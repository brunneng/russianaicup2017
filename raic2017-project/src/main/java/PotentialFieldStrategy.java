import model.Game;
import model.Move;

import java.util.List;

/**
 * User: goodg_000
 * Date: 19.11.2017
 * Time: 15:10
 */
public class PotentialFieldStrategy extends BattleStrategy<PotentialFieldStrategyInfo> {

   public PotentialFieldStrategy(Game game, WorldInfo world, List<Group> groups, Battlefield battlefield) {
      super(game, world, groups, battlefield,false);
   }

   @Override
   public boolean apply(Move move) {

      GroupMoveResult bestGroupMoveResult = null;
      double bestScore = Integer.MIN_VALUE;
      for (Group g : getStrategyCandidatesGroups()) {
         GroupMoveResult groupMoveResult = findBestAction(g);
         double score = groupMoveResult.getScoreDiff()
                    /* + (world.getTick() - getGroupStrategyInfo(g).getStartStrategyTick())*0.005*/;
         if (bestGroupMoveResult == null || score > bestScore) {
            bestGroupMoveResult = groupMoveResult;
            bestScore = score;
         }
      }

      if (bestGroupMoveResult != null) {
         final GroupMoveResult res = bestGroupMoveResult;
         return selectAndCallTrigger(bestGroupMoveResult.getGroup(), move, new SelectGroupActionStrategyTrigger() {
            @Override
            public boolean apply(Move move) {
               return performGroupAction(res, move);
            }
         });
      }

      return false;
   }

   protected GroupMoveResult findBestAction(Group g) {
      return battlefield.findBestAction(g);
   }

   protected boolean performGroupAction(GroupMoveResult bestGroupAction, Move move) {
      Group group = bestGroupAction.getGroup();
      performMoveAction(group, bestGroupAction.getTarget(), bestGroupAction.getTargetWithShift(), move);
      finishStrategyForGroup(group);
      return true;
   }

   @Override
   public BattleStrategyType getType() {
      return BattleStrategyType.PotentialField;
   }

   @Override
   protected PotentialFieldStrategyInfo createGroupStrategyInfo() {
      return new PotentialFieldStrategyInfo();
   }
}
