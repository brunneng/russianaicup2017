import model.*;

public final class MyStrategy implements Strategy {

   private Army myArmy = new Army();
   private long totalTime;
   private double totalMillisPerTick;

   @Override
   public void move(Player me, World world, Game game, Move move) {
      Draw.startTick();
      long start = System.currentTimeMillis();
      myArmy.update(world, game);
      myArmy.doMove(move);
      long time = System.currentTimeMillis() - start;
      totalTime += time;
      int tick = world.getTickIndex() + 1;
      totalMillisPerTick = totalTime / (double)tick;
      Draw.endTick();
   }
}
