import model.Facility;
import model.VehicleType;

import java.util.List;

/**
 * User: goodg_000
 * Date: 09.11.2017
 * Time: 20:18
 */
public class CreateGroupRequest {
   private Integer groupId;
   private List<BattleUnit> unitsToSelect;
   private VehicleType vehicleType;
   private long selectedTick = -1;
   private Group group;
   private Facility producedFacility;

   public CreateGroupRequest(List<BattleUnit> unitsToSelect, VehicleType vehicleType) {
      this.unitsToSelect = unitsToSelect;
      this.vehicleType = vehicleType;
   }

   public Integer getGroupId() {
      return groupId;
   }

   public void setGroupId(Integer groupId) {
      this.groupId = groupId;
   }

   public List<BattleUnit> getUnitsToSelect() {
      return unitsToSelect;
   }

   public void setUnitsToSelect(List<BattleUnit> unitsToSelect) {
      this.unitsToSelect = unitsToSelect;
   }

   public boolean isSelected() {
      return selectedTick == Army.getLastSelectTick();
   }

   public void setSelected() {
      this.selectedTick = Army.world.getTick();
   }

   public VehicleType getVehicleType() {
      return vehicleType;
   }

   public void setVehicleType(VehicleType vehicleType) {
      this.vehicleType = vehicleType;
   }

   public void setGroupId(int groupId) {
      this.groupId = groupId;
   }

   public Group getGroup() {
      return group;
   }

   public void setGroup(Group group) {
      this.group = group;
   }

   public Facility getProducedFacility() {
      return producedFacility;
   }

   public void setProducedFacility(Facility producedFacility) {
      this.producedFacility = producedFacility;
   }
}
