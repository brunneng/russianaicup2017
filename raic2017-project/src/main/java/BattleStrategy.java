import model.ActionType;
import model.Game;
import model.Move;
import model.VehicleType;

import java.util.List;
import java.util.stream.Collectors;

/**
 * User: goodg_000
 * Date: 19.11.2017
 * Time: 12:56
 */
public abstract class BattleStrategy<GS extends GroupStrategyInfo> {

   protected final Game game;
   protected final WorldInfo world;
   protected final List<Group> groups;
   protected final Battlefield battlefield;
   private final boolean handleEmergencyMove;

   public BattleStrategy(Game game, WorldInfo world, List<Group> groups, Battlefield battlefield,
                         boolean handleEmergencyMove) {
      this.game = game;
      this.world = world;
      this.groups = groups;
      this.battlefield = battlefield;
      this.handleEmergencyMove = handleEmergencyMove;
   }

   public boolean apply(Move move, boolean emergencyMove) {
      if (emergencyMove && !handleEmergencyMove) {
         return false;
      }

      for (Group g : groups) {
         if (isCurrentStrategy(g)) {
            GS gs = getGroupStrategyInfo(g);
            GroupStrategyTrigger trigger = gs.getTrigger();
            if (trigger != null && trigger.isShouldApply()) {
               boolean movePerformed = trigger.apply(move);
               if (trigger == gs.getTrigger()) {
                  gs.setTrigger(null);
               }
               if (movePerformed) {
                  return true;
               }
            }
         }
      }

      return apply(move);
   }

   public List<Group> getCurrentStrategyGroups() {
      return groups.stream().filter(this::isCurrentStrategy).collect(Collectors.toList());
   }

   public List<Group> getStrategyCandidatesGroups() {
      return groups.stream().filter(g -> !isGroupBusy(g) && !isCurrentStrategy(g)).collect(Collectors.toList());
   }

   public List<Group> getStrategyCandidatesWithCurrent() {
      return groups.stream().filter(g -> !isGroupBusy(g)).collect(Collectors.toList());
   }

   protected abstract boolean apply(Move move);

   public abstract BattleStrategyType getType();

   protected abstract GS createGroupStrategyInfo();

   protected GS getGroupStrategyInfo(Group g) {
      GS gs = (GS) g.getGroupStrategyInfo(getType());
      if (gs == null) {
         gs = createGroupStrategyInfo();
         g.setGroupStrategyInfo(getType(), gs);
      }
      return gs;
   }

   protected boolean isGroupBusy(Group g) {
      BattleStrategyType currentStrategyType = g.getCurrentStrategyType();
      return currentStrategyType != null && currentStrategyType.getPriority() < getType().getPriority();
   }

   protected boolean isCurrentStrategy(Group g) {
      return getType().equals(g.getCurrentStrategyType());
   }

   protected void finishStrategyForGroups() {
      for (Group g : groups) {
         finishStrategyForGroup(g);
      }
   }

   protected void finishStrategyForGroup(Group g) {
      if (isCurrentStrategy(g)) {
         g.setCurrentStrategyType(null);
         getGroupStrategyInfo(g).setEndStrategyTick(Army.world.getTick());
      }
   }

   protected GS setStrategyForGroup(Group g) {
      if (!isGroupBusy(g)) {
         if (g.getCurrentStrategyType() != null) {
            g.getGroupStrategyInfo(g.getCurrentStrategyType()).setEndStrategyTick(Army.world.getTick());
         }

         g.setCurrentStrategyType(getType());
         GS gs = createGroupStrategyInfo();
         g.setGroupStrategyInfo(getType(), gs);
         gs.restartStrategy();
         return gs;
      }
      return null;
   }

   protected double getAngleToRotate(Group g, Vector2D moveVector) {
      double rotate1 = Vector2D.fromAngleAndLength(g.getPointsAxis() + Math.PI / 2,
              1).angleToVector(moveVector);

      double rotate2 = Vector2D.fromAngleAndLength(g.getPointsAxis() - Math.PI / 2,
              1).angleToVector(moveVector);

      return Math.abs(rotate1) < Math.abs(rotate2) ? rotate1 : rotate2;
   }

   protected int getTicksToRotate(Group g, double angle) {
      return (int) (Utils.calcArcLength(g.getGroupRadius(), angle) / Utils.getSpeed(g.getVehicleType()));
   }

   protected boolean performMoveAction(Group group, Point target, Point targetWithShift, Move move) {
      group.setMoveTarget(target);
      group.setMoveTargetWithShift(targetWithShift);

      double densityFactor = group.calcDensityFactor();
      Point currPos = group.getPos();
      Vector2D moveVector = new Vector2D(currPos, targetWithShift);

      Draw.move(new LastDrawMove(currPos, targetWithShift));
      doMove(group, moveVector, densityFactor, move);

      return true;
   }

   private boolean isRequireRestrictSpeedOnPath(Group g) {
      if (g.getVehicleType().equals(VehicleType.FIGHTER)) {
         return true;
      }
      if (g.getVehicleType().equals(VehicleType.HELICOPTER)) {
         return true;
      }
//      if (g.size() < 40 && g.calcDensityFactor() < 5.0) {
//         return true;
//      }
      return false;
   }

   private void doMove(Group g, Vector2D moveVector, double densityFactor, Move move) {

      double minSpeedOfUnit = Integer.MAX_VALUE;

      if (g.size() > 1) {
         if (isRequireRestrictSpeedOnPath(g)) {
            minSpeedOfUnit = battlefield.findMinGroupSpeedOnPath(g, g.getPos().plus(moveVector));
         }
         else {
            for (BattleUnit b : g.getUnits()) {
               if (b.getMaxSpeed() < minSpeedOfUnit) {
                  minSpeedOfUnit = b.getMaxSpeed();
               }
            }
         }
      }

      move.setMaxSpeed(minSpeedOfUnit);
      double c = getAngleToRotate(g, moveVector);
      if (Math.abs(c) < Math.PI / 9 && densityFactor > 5.0) {
         move.setAction(ActionType.SCALE);
         move.setFactor(0.1);
         moveVector.setLength(moveVector.length() * 1.1);
         Point target = g.getPos().plus(moveVector);
         move.setX(target.getX());
         move.setY(target.getY());
      }
      else {
         move.setAction(ActionType.MOVE);
         move.setX(moveVector.getX());
         move.setY(moveVector.getY());
      }

//      move.setAction(ActionType.MOVE);
//      move.setX(moveVector.getX());
//      move.setY(moveVector.getY());
   }

   protected void performRotateAction(Group group, Point target, double angle, Move move) {
      Point currPos = group.getPos();

      move.setAction(ActionType.ROTATE);

      move.setX(currPos.getX());
      move.setY(currPos.getY());

      move.setAngle(angle);

      move.setX(target.getX());
      move.setY(target.getY());
   }

   protected boolean selectAndCallTrigger(Group group, Move move, SelectGroupActionStrategyTrigger trigger) {
      if (!isCurrentStrategy(group)) {
         setStrategyForGroup(group);
      }

      if (group.isSelected()) {
         return trigger.apply(move);
      }
      selectGroup(group, move);
      getGroupStrategyInfo(group).setTrigger(trigger);
      return true;
   }

   protected void selectGroup(Group group, Move move) {
      for (Group g : groups) {
         if (g.isSelected() && g != group) {
            g.setSelected(false);
         }
      }

      group.setSelected(true);
      move.setAction(ActionType.CLEAR_AND_SELECT);
      move.setGroup(group.getId());
   }

   protected void addGroupToSelection(Group group, Move move) {
      group.setSelected(true);
      move.setAction(ActionType.ADD_TO_SELECTION);
      move.setGroup(group.getId());
   }
}
